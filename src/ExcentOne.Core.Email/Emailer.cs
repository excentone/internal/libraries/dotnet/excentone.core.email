﻿using System;
using System.ComponentModel;
using System.Net;
using System.Net.Mail;
using ExcentOne.Core.Configuration.Settings;
using ExcentOne.Extensions.Text;
using Microsoft.Extensions.Logging;
using Serilog;

namespace ExcentOne.Core.Email
{
    public static class Emailer
    {
        public static bool SendEmail(Smtp smtp, Data.Email email)
        {
            //return false ? SendEmailUsingEws(smtp, email) : SendEmailUsingDirectO365(smtp, email);
            return SendEmailUsingDirectO365(smtp, email);
        }

        private static void HandleEmailAddress(string recipient, MailAddressCollection col)
        {
            //check if can be split
            var arrRec = recipient.Split(new[] {';', ','}, StringSplitOptions.RemoveEmptyEntries);
            foreach (var rec in arrRec) col.Add(rec);
        }

        private static bool SendEmailUsingDirectO365(Smtp smtp, Data.Email email)
        {
            var smtpClient = new SmtpClient();

            if (smtp.UseLocal)
            {
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis;
            }
            else
            {
                smtpClient.UseDefaultCredentials = smtp.UseDefaultCredentials;
                if (!smtp.UseDefaultCredentials)
                    smtpClient.Credentials = !smtp.Domain.Equals(string.Empty)
                        ? new NetworkCredential(smtp.Username, smtp.EncryptedPassword, smtp.Domain)
                        : new NetworkCredential(smtp.Username, smtp.EncryptedPassword);

                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpClient.Host = smtp.Host;
                smtpClient.Port = smtp.Port;
                smtpClient.EnableSsl = smtp.EnableSsl;

                if (!smtp.TargetName.Equals(string.Empty))
                    smtpClient.TargetName = smtp.TargetName;
            }

            if (email.From.IsNullOrWhiteSpace()) email.From = smtp.Username;

            var mailMessage = new MailMessage
            {
                Body = email.Body,
                From =
                    email.FromName.IsNullOrWhiteSpace() || email.FromName.Equals(string.Empty)
                        ? new MailAddress(email.From)
                        : new MailAddress(email.From, email.FromName),
                IsBodyHtml = email.IsHtml,
                Subject = email.Subject
            };

            if (!email.To.Equals(string.Empty))
                HandleEmailAddress(email.To, email.ToCollection); //mailMessage.To.Add(new MailAddress(email.To));
            foreach (var to in email.ToCollection) mailMessage.To.Add(to);

            if (!email.Cc.Equals(string.Empty))
                HandleEmailAddress(email.Cc, email.CcCollection); //mailMessage.CC.Add(email.Cc);
            foreach (var cc in email.CcCollection) mailMessage.CC.Add(cc.Address);

            if (!email.Bcc.Equals(string.Empty))
                HandleEmailAddress(email.Bcc, email.BccCollection); //mailMessage.Bcc.Add(email.Bcc);
            foreach (var bcc in email.BccCollection) mailMessage.Bcc.Add(bcc.Address);

            smtpClient.SendCompleted += new SendCompletedEventHandler(SendCompletedCallback);
            smtpClient.SendAsync(mailMessage, mailMessage);

            return true;
        }

        private static void SendCompletedCallback(object sender, AsyncCompletedEventArgs e)
        {
            var msg = (MailMessage)e.UserState;

            if (e.Cancelled)
            {
                // prompt user with "send cancelled" message 
                Log.Information("Email is cancelled");
            }
            if (e.Error != null)
            {
                // prompt user with error message 
                Log.Error(e.Error, "Sending of email failed. An error has occurred.");
            }
            else
            {
                Log.Information("Email is sent");
            }

            msg?.Dispose();
        }
    }
}